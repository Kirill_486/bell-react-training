const my_news = [
  {
    author: 'Саша Печкин',
    text: 'В четверг, четвертого числа...'
  },
  {
    author: 'Просто Вася',
    text: 'Считаю, что $ должен стоить 35 рублей!'
  },
  {
    author: 'Гость',
    text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000'
  }
];

const NewsCounter = (props) => (
    <p className="newsCounter">
        <strong>{props.newsCount > 0 ? `Новостей ${props.newsCount}` : 'Новостей нет'}</strong>
    </p>
);

class AddNewsForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            errorMessage : "",
            author: "",
            text: "",
            isFirstCheck: true
        }

        this.onAuthorChange = this.onAuthorChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onContentChange = this.onContentChange.bind(this);

        this.authorIsEmptyMessage = 'Поле автор author обязательно для заполнения!';
    }   

    onAuthorChange(e) {
        const author = e.target.value;
        if (author) {            
            this.setState({
                author,
                errorMessage: "",
                isFirstCheck: false
            });
        } else {            
            this.setState({
                errorMessage : this.authorIsEmptyMessage
            });            
        }
    }

    onSubmit(e) {
        e.preventDefault();
        console.log(!this.state.errorMessage && !this.state.isFirstCheck);
        if (!this.state.errorMessage && !this.state.isFirstCheck) {
            this.props.addNews(this.state);
        } else {
            this.setState({
                errorMessage: this.authorIsEmptyMessage,
                isFirstCheck: false
            })
        }
    }

    onContentChange(e) {
        const text = e.target.value;
        this.setState({
            text
        });
    }

    render() {
        return (
            <form className="addNewsForm" onSubmit = {this.onSubmit}>
                {this.state.errorMessage && <div className="addNewsForm__error">{this.state.errorMessage}</div> }
                <div className="addNewsForm__group">
                    <label className="addNewsForm__group__label" htmlFor="author">author</label>
                    <input 
                        type="text" 
                        name="author"
                        onChange={this.onAuthorChange}                    
                        />
                </div>
                <div className="addNewsForm__group">
                    <label className="addNewsForm__group__label" htmlFor="text">Текст новости</label>
                    <textarea name="text" cols="30" rows="10" onChange={this.onContentChange}></textarea>
                </div>
                <div className="addNewsForm__group">
                    <input type="submit" value="Добавить новость"/>
                </div>
            </form>
        );
    }
}
    
const News = (props) => (
    <div className="newsDashboard">
        {props.news
            .map((item, index) => 
                <Article 
                    key={index}
                    recordKey={index}
                    author={item.author} 
                    text={item.text}
                />)
            } 
            
        <NewsCounter  newsCount={props.news.length}/>               
    </div>
); 

const Article = (props) => (
    <div className="article" key={props.recordKey}>
        <span>{props.author}</span> : <span>{props.text}</span>
    </div>
);

class App extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            news : my_news
        }
        this.addNews = this.addNews.bind(this);
    }

    addNews({author, text}) {
        const newRecord = {
            author,
            text
        }
        
        this.setState({
            news : [...this.state.news, newRecord]
        });
    }

    render() {
        return (
        <div>
            <h1>News app by Kirill Penkin</h1>
            <AddNewsForm addNews={this.addNews} />
            <News news={this.state.news}/>
        </div>
        );
    }
}

const appRoot = document.querySelector('#app');


ReactDOM.render(<App/>, appRoot);